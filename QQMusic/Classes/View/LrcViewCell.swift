//
//  LrcViewCell.swift
//  QQMusic
//
//  Created by hjj on 2016/12/3.
//  Copyright © 2016年 hjjuny. All rights reserved.
//

import UIKit

class LrcViewCell: UITableViewCell {
    
    lazy var lrcLabel : LrcLabel = LrcLabel()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(lrcLabel)
        
        backgroundColor = UIColor.clear
        selectionStyle = .none
        
        lrcLabel.textColor = UIColor.white
        lrcLabel.font = UIFont.systemFont(ofSize: 14)
        lrcLabel.textAlignment = .center
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        lrcLabel.sizeToFit()
        lrcLabel.center = contentView.center
    }
}
