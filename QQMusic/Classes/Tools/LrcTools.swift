//
//  LrcTools.swift
//  QQMusic
//
//  Created by hjj on 2016/12/3.
//  Copyright © 2016年 hjjuny. All rights reserved.
//

import UIKit

class LrcTools: NSObject {
    
}

extension LrcTools {
    class func parseLrc(_ lrcname : String) -> [LrclineModel]? {
        // 1.获取路径
        guard let path = Bundle.main.path(forResource: lrcname, ofType: nil) else {
            return nil
        }
        
        // 2.读取路径中的内容
        guard let totalLrcString = try? String(contentsOfFile: path) else {
            return nil
        }
        
        // 3.对字符串进行分割
        let lrclineStrings = totalLrcString.components(separatedBy: "\n")
        var lrclines : [LrclineModel] = [LrclineModel]()
        for lrclineStr in lrclineStrings {
            // [01:04.98]就像已伤的心　不胜折磨
            // 3.1.过滤不需要的行数
            if lrclineStr.contains("[ti:") || lrclineStr.contains("[ar:") || lrclineStr.contains("[al:") || !lrclineStr.contains("[") {
                continue
            }
            
            // 3.2.取出歌词
            let lrclineModel = LrclineModel(lrclineString: lrclineStr)
            lrclines.append(lrclineModel)
        }
        
        return lrclines
    }
}
