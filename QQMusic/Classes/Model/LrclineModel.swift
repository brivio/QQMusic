//
//  LrclineModel.swift
//  QQMusic
//
//  Created by hjj on 2016/12/3.
//  Copyright © 2016年 hjjuny. All rights reserved.
//

import UIKit

class LrclineModel: NSObject {
    var lrcText : String = ""
    var lrcTime : TimeInterval = 0
    
    init(lrclineString : String) {
        // [00:35.89]你所有承诺　虽然都太脆弱
        let lrclineStrs = lrclineString.components(separatedBy: "]")
        lrcText = lrclineStrs[1]
        // 00:35.89
        let lrclineTimeStr = lrclineStrs[0].components(separatedBy: "[")[1]
        
        let min = Double(lrclineTimeStr.components(separatedBy: ":")[0])!
        let second = Double(lrclineTimeStr.components(separatedBy: ":")[1].components(separatedBy: ".")[0])!
        let haomiao = Double(lrclineTimeStr.components(separatedBy: ".")[1])!
        
        lrcTime = min * 60 + second + haomiao * 0.01
    }
}
